#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 10 23:14:02 2020

@author: areenn
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import sklearn
import sklearn.cluster
from matplotlib.lines import Line2D 
from sklearn import model_selection
import sklearn.metrics
import sklearn.tree
import sklearn.ensemble
import sklearn.neighbors
from sklearn.neural_network import MLPClassifier
pd.options.mode.chained_assignment = None

def score(pred,t1,t2,flag=0):
    corr = 0
    for i in range(len(pred)):
        if flag:
            if (pred[i] == 1 and t1[i] == 1) or (pred[i] == 1 and t2[i] == 1):
                corr += 1
        else:
            if (pred[i] == t1[i]) or (pred[i] == t2[i]):
                corr += 1 
    return([corr,corr/len(pred)])

def optimize_MLP(target,typ1,typ2,x,y):
    results = []
    for i in range(1,x):
        for j in range(1,y):
                MLP = MLPClassifier(solver='lbfgs',hidden_layer_sizes=(i,j),random_state=2137,max_iter=100000)
                MLP.fit(target,typ1)
                MLP_pred = MLP.predict(target)
                results.append([i] + [j] + score(MLP_pred,typ1,typ2))
    results = np.array(results)
    out = results[np.where(results[:,3] == max(results[:,3])),:]
    return tuple([int(out[0][0][0]),int(out[0][0][1])])


def optimize_knn(target,typ1,typ2,x):
    results = []
    params = []
    for i in range(1,x):
        for j in ['uniform','distance']:
                knn = sklearn.neighbors.KNeighborsClassifier(n_neighbors=i,weights=j)
                knn.fit(target,typ1)
                knn_pred = knn.predict(target)
                params.append([i,j])
                results.append(score(knn_pred,typ1,typ2)[1])
    m = max(results)
    out = [i for i,j in enumerate(results) if j==m ]
    if len(out)>1:
        out = out[len(out)//4]
    return params[out]




def learn(u,df1,category,flag=0):
    if category=='type1':
        nots = df1[df1[category] != u]
        yess = df1[df1[category] == u]
        idx_n_u, idx_n_t = sklearn.model_selection.train_test_split(np.arange(nots.shape[0]), test_size=0.2, random_state=2137)
        idx_y_u, idx_y_t = sklearn.model_selection.train_test_split(np.arange(yess.shape[0]), test_size=0.2, random_state=2137)
        training = pd.concat([nots.iloc[idx_n_u,:],yess.iloc[idx_y_u,:]],axis=0)
        target = pd.concat([nots.iloc[idx_n_t,:],yess.iloc[idx_y_t,:]],axis=0)
        training = training.sample(frac=1) #mixing up input, to not have it divided in two
        target = target.sample(frac=1)

        tr_typ_c = (training['type1'] == u).astype(int).values
        tar_typ_c = (target['type1'] == u).astype(int).values
        tar_typ2_c = (target['type2'] == u).astype(int).values
        
        training = training[['weight_kg','hp','attack','sp_attack','defense','sp_defense','speed']]
        #training =  (training - training.min())/(training.max()-training.min()) #normalizacja stats
        training = (training - training.mean())/training.std() #standaryzacja

        
        target = target[['weight_kg','hp','attack','sp_attack','defense','sp_defense','speed']]
        #target =  (target - target.min())/(target.max()-target.min()) #normalizacja stats
        target = (target - target.mean())/target.std() #standaryzacja
        
    else:
        df2 = df1[df1[category] != u]
        df3 = df1[df1[category] == u]
        target = df3[['weight_kg','hp','attack','sp_attack','defense','sp_defense','speed']]
        #target =  (target - target.min())/(target.max()-target.min()) #normalizacja stats
        target = (target - target.mean())/target.std() #standaryzacja
    
        training = df2[['weight_kg','hp','attack','sp_attack','defense','sp_defense','speed']]
        #training =  (training - training.min())/(training.max()-training.min()) #normalizacja stats
        training = (training - training.mean())/training.std() #standaryzacja

        #df2.type1 = df2.type1.astype("category")
        tr_typ_c = df2['type1'].cat.codes.values
        tar_typ_c = df3['type1'].cat.codes.values
        tar_typ2_c = df3['type2'].cat.codes.values
    
    out = [u]
    
    knn = sklearn.neighbors.KNeighborsClassifier() #5-nn
    knn.set_params(**knn_p)
    knn.fit(training, tr_typ_c) 
    knn_pred = knn.predict(target)
    out = out + score(knn_pred,tar_typ_c,tar_typ2_c,flag)
    
    las = sklearn.ensemble.RandomForestClassifier(random_state=123)
    las.set_params(**las_p)
    las.fit(training, tr_typ_c)
    las_pred = las.predict(target)
    out = out + score(las_pred,tar_typ_c,tar_typ2_c,flag)
    
    MLP = MLPClassifier(solver='lbfgs',hidden_layer_sizes=(9,8),random_state=2137,max_iter=100000)
    MLP.set_params(**MLP_p)
    MLP.fit(training, tr_typ_c)
    MLP_pred = MLP.predict(target)
    out = out + score(MLP_pred,tar_typ_c,tar_typ2_c,flag)
    print("Generacja {}\n\t Ułamek poprawnych\t\nkNN\t {}\t{:.3f}\nRF\t{}\t{:.3f}\nMLP\t{}\t{:.3f}\n".format(*out))
    
def plotter(target):
    bars = np.linspace(1,18,18)
    fig, axes = plt.subplots(nrows=6, ncols=3, sharex='col', figsize=(24,9))
    fig.add_subplot(111, frameon=False)
    for i, ax in enumerate(axes.flatten()):
        tmp_df = target[target.clustered == i].groupby('type1').size()
        tmp_df2 = target[target.clustered == i].groupby('type2').size()
        amount = sum(tmp_df)
        tmp_df = (tmp_df*100)/sum(tmp_df)
        tmp_df2 = (tmp_df2*100)/sum(tmp_df2)
        for q in types:
            if q not in tmp_df.index:
                u = pd.Series([0], index=[q])
                tmp_df.add(u)
        for q in types:
            if q not in tmp_df2.index:
                u = pd.Series([0], index=[q])
                tmp_df2.add(u)
        
        ax.set_title('Klaster numer ' + str(i))
        ax.bar(bars-0.2,tmp_df.values,width=0.4)
        ax.bar(bars+0.2,tmp_df2.values,width=0.4)
        ax.set_xticks(bars)
        ax.set_xticklabels(tmp_df.index)
        ax.tick_params(rotation=60)
        ax.yaxis.set_label_position("right")
        ax.set_ylabel('Liczność ='+str(amount))
    fig.subplots_adjust(wspace=0.3,hspace=0.5)
    plt.tick_params(labelcolor='none', top=False, bottom=False, left=False, right=False)
    plt.ylabel('Procent pokemonów danego typu w klastrze',fontsize=18)
    plt.xlabel('Typy pokemonów',labelpad=30,fontsize=18)
    name= 'all_gen_std_weight.pdf'
    prop_cycle = plt.rcParams['axes.prop_cycle']
    colors = prop_cycle.by_key()['color']
    clines = [Line2D([0],[0],lw=4,color=colors[0]),
              Line2D([0],[0],lw=4,color=colors[1])]
    plt.legend(handles=clines,labels=['Typ pierwszorzędny','Typ drugorzędny'],loc = 'lower left', ncol=2, bbox_to_anchor=(0,-0.15))
    plt.savefig(name,dpi=300,transparent=False,bbox_inches='tight',format='pdf')
    plt.clf()

def classify_all(df,typ1,typ2):
    
    #parms1 = optimize_knn(df,typ1,typ2,20)
    #print("Optymalnie, liczba najbliższych sąsiadów branych pod uwagę to {}, a wagi to {}.".format(*parms1))
    knn = sklearn.neighbors.KNeighborsClassifier(n_neighbors=5,weights='distance')
    knn.fit(df,typ1)
    knn_pred = knn.predict(df)
    out = score(knn_pred,typ1,typ2)
    
    las = sklearn.ensemble.RandomForestClassifier(random_state=123)
    las.fit(df, typ1)
    las_pred = las.predict(df)
    out = out + score(las_pred,typ1,typ2)
    
    #parms2 = optimize_MLP(df,typ1,typ2,10,10)
    #print("Optymalna dla całego zbioru liczba warstw to {}, a węzłów na warstwę to {}.".format(*parms2))
    MLP = MLPClassifier(solver='lbfgs',hidden_layer_sizes=(9,8),random_state=2137,max_iter=100000)
    MLP.fit(df, typ1)
    MLP_pred = MLP.predict(df)
    out = out + score(MLP_pred,typ1,typ2)
    print("Wszystkie pokemony\n\t Ułamek poprawnych\t\nkNN\t {}\t{:.3f}\nRF\t{}\t{:.3f}\nMLP\t{}\t{:.3f}\n".format(*out))
    return knn.get_params(),las.get_params(),MLP.get_params()


df = pd.read_csv("pokemon.csv", comment="#")
print(df.info())
type_dict = {}
df.type1 = df.type1.astype("category")
df.type2 = df.type2.astype("category")
types = df.groupby('type1').size().index
df1= df[df.is_legendary == 0] 
print(sum(df1['type2'].isna())) #liczba pokemonow bez podwojnego typu
df1.loc[df1['type2'].isna(), 'type2'] = df1['type1']
kn = df1[['weight_kg','hp','attack','sp_attack','defense','sp_defense','speed']]
#kn =  (kn - kn.min())/(kn.max()-kn.min()) #normalizacja stats
kn = (kn - kn.mean())/kn.std() #standaryzacja
#sns.heatmap(cbar=True, annot=True,data = kn.corr(),cmap='coolwarm')
#plt.show()
df2 = kn.copy()
km = sklearn.cluster.KMeans(n_clusters=18, random_state=123) 
y_pred = km.fit_predict(kn)
kn['clustered'] = y_pred
kn['type1']  = df1['type1']
kn['type2']  = df1['type2']
kn['name']  = df1['name']
plotter(kn)#wyniki K-means cluster

knn_p, las_p, MLP_p = classify_all(df2,df1.type1.cat.codes.values,df1.type2.cat.codes.values)

for i in range(1,8):    #klasyfikacja n-tej generacji na podstawie pozostałch
    learn(i,df1,'generation')

for i in ['bug','ice','poison','normal','fire','water','ground']:
    learn(i,df1,'type1',1)




  